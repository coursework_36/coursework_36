# Coursework_36
Team "Coursework_36" consist of following members:
Akshay Goud Batti  							-- 18021057
Gandavarapu Sri Sai Yasaswini				-- 18053213
Kapilavai Naga Anjaneya Sai Balakrishna		-- 18053187
Tirumalasetty Mounika						-- 18021624
Sandeep Perugu								-- 18058767 	

**Note : The files(not all) which are submitted in the canvas can be found in the folder "File_Report&Analysis_Files" with file names as 
"Coursework_36_Final_Report"
"Course_36_Final_Analysis_Dataset"
"Original_dataset"

"Coursework_36" is a team of five people doing coursework on 7COM1079-2019 Team Research and Development Project under the Module leader: Dr. John Noll, Sr. Lecturer, School of Engineering and Computer Science.

As a part of coursework, we divided our work and shared each individual work in the Trello Board as "Coursework 36" and BitBucket repository as "https://akshaygoudbatti@bitbucket.org/coursework_36/coursework_36.git".

which are reviewed and merged together to make a final report and are submitted through canvas.

About our Dataset:

the dataset we choose from Kaggle dataset ( link : https://www.kaggle.com/uciml/student-alcohol-consumption )

the dataset is based on survey of students math and portuguese language courses in secondary school. It contains a lot of interesting social, gender and study information about students.

as there is raw dataset with name "student-alcohol-consumption.zip"




datset source from:
https://www.kaggle.com/uciml/student-alcohol-consumption

P. Cortez and A. Silva. Using Data Mining to Predict Secondary School Student Performance. In A. Brito and J. Teixeira Eds., Proceedings of 5th FUture BUsiness TEChnology Conference (FUBUTEC 2008) pp. 5-12, Porto, Portugal, April, 2008, EUROSIS, ISBN 978-9077381-39-7.


Fabio Pagnotta, Hossain Mohammad Amran. Email:fabio.pagnotta@studenti.unicam.it, mohammadamra.hossain '@' studenti.unicam.it University Of Camerino



Contributer for this Research report :

1) Akshay Goud Batti  
	1. Creating and maintaining Repository and Trello Board. 
	2. Dataset Selection.
	3. Research question on "Question 1 Does weekday alcohol consumption impact the scores of the students in maths?".
	4. Introduction and Conclusion in Research report.
	5. Merging the research reports from team members into a Final Report.
	6. Inviting Dr. John Noll to Repository and Trell Board "Coursework 36".
	7. Submitting the whole assignment into the canvas on behalf of my team members.
	
2) Mounika Tirumalasetty
	1. Research Report on "Question 2 Does weekend alcohol consumption impact the scores of the students in maths?".
	2. Reviewing Report format.
	3. Cleaning Dataset
	
3) Sri Sai Yasaswini Gandavarapu
	1. Reviewing the trello.
	2. Research report on "Question 3: Does weekday alcohol consumption impact the scores of the students in Portuguese?".
	3. Final report review.
	
4)Kapilavai Naga Anjaneya Sai Balakrishna
	1. Research report on "Question 4: Does weekend alcohol consumption impact the scores of the students in Portuguese?"
	2. Reviewing Dataset and format
	3. Reviewing Final Dataset
5)Sandeep Perugu
	1. Research report on topics "Background", "Methods" and "Conclusion"
	2. Dataset extension review